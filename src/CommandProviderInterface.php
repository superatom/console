<?php namespace Superatom\Console;

interface CommandProviderInterface
{
    /**
     * Register console commands
     *
     * @param Application $application
     */
    public function register(Application $application);
}