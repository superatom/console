<?php namespace Superatom\Console;

use Illuminate\Filesystem\Filesystem;
use Pimple\Container;
use Superatom\Console\Commands\ReplCommand;
use Symfony\Component\Console\Command\Command as SymfonyCommand;

class Application extends \Symfony\Component\Console\Application
{
    /**
     * Pimple DI container instance
     *
     * @var Container
     */
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $this->prepareContainer($container);

        parent::__construct();

        $this->setAutoExit(false);
    }

    public function add(SymfonyCommand $command)
    {
        if ($command instanceof Command) {
            $command->setContainer($this->container);
        }

        return parent::add($command);
    }

    /**
     * Register command providers
     *
     * @param array $providers
     */
    public function registerProviders(array $providers)
    {
        foreach ($providers as $provider) {
            /** @var CommandProviderInterface $class */
            $class = new $provider;
            $class->register($this);
        }
    }

    /**
     * Get DI container instance
     *
     * @return Container
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Get Filesystem instance from container
     *
     * @return Filesystem
     */
    public function getFilesystem()
    {
        return $this->container['files'];
    }

    /**
     * Get Composer instance from container
     *
     * @return Composer
     */
    public function getComposer()
    {
        return $this->container['composer'];
    }

    /**
     * Prepare default dependencies
     *
     * @param Container $container
     * @return Container
     */
    protected function prepareContainer(Container $container)
    {
        if ( ! isset($container['files'])) {
            $container['files'] = new Filesystem();
        }

        if ( ! isset($container['composer'])) {
            $container['composer'] = new Composer($container['files'], getcwd());
        }

        return $container;
    }

    protected function getDefaultCommands()
    {
        $commands = parent::getDefaultCommands();
        $commands[] = new ReplCommand();

        return $commands;
    }
}