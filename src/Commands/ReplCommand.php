<?php namespace Superatom\Console\Commands;

use Psy\Shell;
use Superatom\Console\Command;

class ReplCommand extends Command
{
    public function handle()
    {
        $shell = new Shell();
        $shell->run();
    }

    protected function configure()
    {
        $this
            ->setName('repl')
            ->setDescription('Run REPL console for your application')
        ;
    }
}