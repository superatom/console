<?php namespace Superatom\Console\Commands;

class CommandMakeCommand extends GeneratorCommand
{
    /**
     * @var string
     */
    protected $namespace;

    public function __construct($outputDir, $namespace)
    {
        parent::__construct($outputDir);

        $this->namespace = $namespace;
    }

    /**
     * get stub text
     *
     * @return string
     */
    public function getStub()
    {
        return $this->getApplication()->getFilesystem()->get(__DIR__ . '/stubs/command.stub');
    }

    public function handle()
    {
        $replacement = [];
        $replacement['DummyClass'] = $class = $this->argument('name');
        $replacement['DummyNamespace'] = $this->namespace;

        $filename = $class . '.php';
        $this->comment('output: '.$this->getOutputDir() . '/' . $filename);

        if ($this->generate($filename, $replacement)) {
            $this->info('Command created successfully.');
        } else {
            $this->error('Command already exists.');
        }
    }

    protected function configure()
    {
        $this
            ->setName('make:command')
            ->setDescription('Create a new command')
            ->addArgument('name', 'The name of the command class')
        ;
    }
}