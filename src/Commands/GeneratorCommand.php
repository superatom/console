<?php namespace Superatom\Console\Commands;

use Superatom\Console\Command;

abstract class GeneratorCommand extends Command
{
    /**
     * Path to output directory
     *
     * @var string
     */
    protected $outputDir;

    public function __construct($outputDir)
    {
        parent::__construct();
        $this->outputDir = rtrim($outputDir, '/');
    }

    /**
     * get stub text
     *
     * @return string
     */
    abstract public function getStub();

    /**
     * @param string $filename
     * @param array $replacement
     * @return bool
     */
    public function generate($filename, array $replacement)
    {
        $stub = $this->formatStub($this->getStub(), $replacement);

        return $this->store($filename, $stub);
    }

    public function getOutputDir()
    {
        $dir = $this->outputDir;

        if ( ! is_dir($dir)) {
            throw new \RuntimeException("Directory not exists or not directory: $dir");
        }

        if ( ! is_writable($dir)) {
            throw new \RuntimeException("Not writable: $dir");
        }

        return $dir;
    }

    /**
     * replace stub text
     *
     * @param string $stub
     * @param array $replacement
     * @return string
     */
    protected function formatStub($stub, array $replacement)
    {
        foreach ($replacement as $src => $dest) {
            $stub = str_replace($src, $dest, $stub);
        }

        return $stub;
    }

    /**
     * save formatted stub text
     *
     * @param string $filename
     * @param string $stub
     * @return bool
     */
    protected function store($filename, $stub)
    {
        $output = $this->getOutputDir() . '/' . $filename;
        if ($this->getApplication()->getFilesystem()->exists($output)) {
            return false;
        }
        $this->getApplication()->getFilesystem()->put($output, $stub);
        return true;
    }
}