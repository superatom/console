<?php namespace Superatom\Console;

use Pimple\Container;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

class Command extends SymfonyCommand
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @var InputInterface
     */
    protected $input;

    /**
     * @var SymfonyStyle
     */
    protected $output;

    /**
     * @param Container $container
     */
    public function setContainer(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @return \Superatom\Console\Application
     */
    public function getApplication()
    {
        return parent::getApplication();
    }

    public function run(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = new SymfonyStyle($input, $output);

        return parent::run($input, $output);
    }

    public function handle()
    {
        return 0;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        return $this->handle();
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        return parent::setName($name);
    }

    /**
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        return parent::setDescription($description);
    }

    /**
     * @param string $name
     * @param string $description
     * @param null $default
     * @param null $mode
     * @return self
     */
    public function addArgument($name, $description = '', $default = null, $mode = null)
    {
        $mode = $mode ?: InputArgument::REQUIRED;

        return parent::addArgument($name, $mode, $description, $default);
    }

    /**
     * @param $name
     * @param string $description
     * @param null $default
     * @return self
     */
    public function addOptionalArgument($name, $description = '', $default = null)
    {
        return parent::addArgument($name, InputArgument::OPTIONAL, $description, $default);
    }

    /**
     * @param string $name
     * @param null $shortcut
     * @param string $description
     * @param null $default
     * @param null $mode
     * @return self
     */
    public function addOption($name, $shortcut = null, $description = '', $default = null, $mode = null)
    {
        $mode = $mode ?: InputOption::VALUE_OPTIONAL;

        return parent::addOption($name, $shortcut, $mode, $description, $default);
    }

    /**
     * @param $name
     * @param null $shortcut
     * @param string $description
     * @param null $default
     * @return self
     */
    public function addBoolOption($name, $shortcut = null, $description = '', $default = null)
    {
        return parent::addOption($name, $shortcut, InputOption::VALUE_NONE, $description, $default);
    }

    /**
     * @param $name
     * @param null $shortcut
     * @param string $description
     * @param null $default
     * @return self
     */
    public function addRequiredOption($name, $shortcut = null, $description = '', $default = null)
    {
        return parent::addOption($name, $shortcut, InputOption::VALUE_REQUIRED, $description, $default);
    }

    /**
     * Call another console command
     *
     * @param string $name
     * @param array $args
     * @return int
     */
    public function call($name, array $args = [])
    {
        $command = $this->getApplication()->find($name);

        $args['command'] = $name;

        return $command->run(new ArrayInput($args), $this->output);
    }

    /**
     * Call another console command silently
     *
     * @param string $name
     * @param array $args
     * @return int
     */
    public function callSilent($name, array $args = [])
    {
        $command = $this->getApplication()->find($name);

        $args['command'] = $name;

        return $command->run(new ArrayInput($args), new NullOutput());
    }

    /**
     * Get the value of a command.php argument.
     *
     * @param  string  $key
     * @return string|array
     */
    public function argument($key = null)
    {
        if (is_null($key)) {
            return $this->input->getArguments();
        }

        return $this->input->getArgument($key);
    }

    /**
     * Get the value of a command.php option.
     *
     * @param  string  $key
     * @return string|array
     */
    public function option($key = null)
    {
        if (is_null($key)) {
            return $this->input->getOptions();
        }

        return $this->input->getOption($key);
    }

    /**
     * Confirm a question with the user.
     *
     * @param  string  $question
     * @param  bool    $default
     * @return bool
     */
    public function confirm($question, $default = false)
    {
        return $this->output->confirm($question, $default);
    }

    /**
     * Prompt the user for input.
     *
     * @param  string  $question
     * @param  string  $default
     * @return string
     */
    public function ask($question, $default = null)
    {
        return $this->output->ask($question, $default);
    }

    /**
     * Prompt the user for input with auto completion.
     *
     * @param  string  $question
     * @param  array   $choices
     * @param  string  $default
     * @return string
     */
    public function anticipate($question, array $choices, $default = null)
    {
        return $this->askWithCompletion($question, $choices, $default);
    }

    /**
     * Prompt the user for input with auto completion.
     *
     * @param  string  $question
     * @param  array   $choices
     * @param  string  $default
     * @return string
     */
    public function askWithCompletion($question, array $choices, $default = null)
    {
        $question = new Question($question, $default);

        $question->setAutocompleterValues($choices);

        return $this->output->askQuestion($question);
    }

    /**
     * Prompt the user for input but hide the answer from the console.
     *
     * @param  string  $question
     * @param  bool    $fallback
     * @return string
     */
    public function secret($question, $fallback = true)
    {
        $question = new Question($question);

        $question->setHidden(true)->setHiddenFallback($fallback);

        return $this->output->askQuestion($question);
    }

    /**
     * Give the user a single choice from an array of answers.
     *
     * @param  string  $question
     * @param  array   $choices
     * @param  string  $default
     * @param  mixed   $attempts
     * @param  bool    $multiple
     * @return bool
     */
    public function choice($question, array $choices, $default = null, $attempts = null, $multiple = null)
    {
        $question = new ChoiceQuestion($question, $choices, $default);

        $question->setMaxAttempts($attempts);
        $question->setMultiselect($multiple);

        return $this->output->askQuestion($question);
    }

    /**
     * Write a string as information output.
     *
     * @param  string  $string
     * @return void
     */
    public function info($string)
    {
        $this->output->writeln("<info>$string</info>");
    }

    /**
     * Write a string as standard output.
     *
     * @param  string  $string
     * @return void
     */
    public function line($string)
    {
        $this->output->writeln($string);
    }

    /**
     * Write a string as comment output.
     *
     * @param  string  $string
     * @return void
     */
    public function comment($string)
    {
        $this->output->writeln("<comment>$string</comment>");
    }

    /**
     * Write a string as question output.
     *
     * @param  string  $string
     * @return void
     */
    public function question($string)
    {
        $this->output->writeln("<question>$string</question>");
    }

    /**
     * Write a string as error output.
     *
     * @param  string  $string
     * @return void
     */
    public function error($string)
    {
        $this->output->writeln("<error>$string</error>");
    }
}